package org.bitbucket.anuarkaliyev23.chess

import org.bitbucket.anuarkaliyev23.chess.exception.ChessException
import org.bitbucket.anuarkaliyev23.chess.game.ClassicGame
import org.bitbucket.anuarkaliyev23.chess.game.GameConsoleGUI
import org.bitbucket.anuarkaliyev23.chess.game.Move
import org.bitbucket.anuarkaliyev23.chess.misc.BoardPosition
import org.bitbucket.anuarkaliyev23.chess.pieces.*
import java.util.*

fun main() {
    val pieces = setOf(
            King(BoardPosition.of("E1"), hasMoved = false, isWhite = true),
//            Queen(BoardPosition.of("D1"), hasMoved = false, isWhite = true),
//            Bishop(BoardPosition.of("C1"), hasMoved = false, isWhite = true),
//            Bishop(BoardPosition.of("F1"), hasMoved = false, isWhite = true),
//            Knight(BoardPosition.of("B1"), hasMoved = false, isWhite = true),
//            Knight(BoardPosition.of("G1"), hasMoved = false, isWhite = true),
            Rook(BoardPosition.of("H1"), hasMoved = false, isWhite = true),
            Rook(BoardPosition.of("A1"), hasMoved = false, isWhite = true),

            Pawn(BoardPosition.of("A2"), hasMoved = false, isWhite = true),
            Pawn(BoardPosition.of("B2"), hasMoved = false, isWhite = true),
            Pawn(BoardPosition.of("C2"), hasMoved = false, isWhite = true),
            Pawn(BoardPosition.of("D2"), hasMoved = false, isWhite = true),
            Pawn(BoardPosition.of("E2"), hasMoved = false, isWhite = true),
            Pawn(BoardPosition.of("F2"), hasMoved = false, isWhite = true),
            Pawn(BoardPosition.of("G2"), hasMoved = false, isWhite = true),
            Pawn(BoardPosition.of("H2"), hasMoved = false, isWhite = true),


            King(BoardPosition.of("E8"), hasMoved = false, isWhite = false),
//            Queen(BoardPosition.of("D8"), hasMoved = false, isWhite = false),
//            Bishop(BoardPosition.of("C8"), hasMoved = false, isWhite = false),
//            Bishop(BoardPosition.of("F8"), hasMoved = false, isWhite = false),
//            Knight(BoardPosition.of("B8"), hasMoved = false, isWhite = false),
//            Knight(BoardPosition.of("G8"), hasMoved = false, isWhite = false),
            Rook(BoardPosition.of("H8"), hasMoved = false, isWhite = false),
            Rook(BoardPosition.of("A8"), hasMoved = false, isWhite = false),

            Pawn(BoardPosition.of("A7"), hasMoved = false, isWhite = false),
            Pawn(BoardPosition.of("B7"), hasMoved = false, isWhite = false),
            Pawn(BoardPosition.of("C7"), hasMoved = false, isWhite = false),
            Pawn(BoardPosition.of("D7"), hasMoved = false, isWhite = false),
            Pawn(BoardPosition.of("E7"), hasMoved = false, isWhite = false),
            Pawn(BoardPosition.of("F7"), hasMoved = false, isWhite = false),
            Pawn(BoardPosition.of("G7"), hasMoved = false, isWhite = false),
            Pawn(BoardPosition.of("H7"), hasMoved = false, isWhite = false)

    )

    val game = ClassicGame(pieces = pieces as MutableSet<Piece>, isWhiteTurn = true)
    val gui = GameConsoleGUI(game)
    val scanner = Scanner(System.`in`)



    gui.show()
    while (true) {
        val move = scanner.nextLine()
        try {
            game.handle(Move.parse(move, game))
        } catch (e : Exception) {
            e.printStackTrace()
            continue
        }
        gui.show()
        println(game.moveHistory)
    }
}