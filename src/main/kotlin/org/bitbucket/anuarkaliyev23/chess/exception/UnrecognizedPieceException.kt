package org.bitbucket.anuarkaliyev23.chess.exception

class UnrecognizedPieceException(msg : String = "") : ChessException(msg)