package org.bitbucket.anuarkaliyev23.chess.exception

open class ChessException(msg: String) : Exception(msg)