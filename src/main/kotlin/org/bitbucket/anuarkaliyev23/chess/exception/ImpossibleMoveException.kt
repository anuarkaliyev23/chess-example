package org.bitbucket.anuarkaliyev23.chess.exception

class ImpossibleMoveException(msg: String = "") : ChessException(msg)