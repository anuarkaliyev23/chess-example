package org.bitbucket.anuarkaliyev23.chess.misc

enum class Horizontal(val numerical: Int) {
    _1(1),
    _2(2),
    _3(3),
    _4(4),
    _5(5),
    _6(6),
    _7(7),
    _8(8);

    operator fun minus(other: Horizontal): Int {
        return this.numerical - other.numerical
    }

    operator fun plus(other: Horizontal) : Int {
        return this.numerical + other.numerical
    }

    operator fun rangeTo(other: Horizontal): Set<Horizontal> {
        return Horizontal
                .values()
                .filterIndexed{_, horizontal ->  horizontal.ordinal >= Math.min(this.ordinal, other.ordinal) && horizontal.ordinal <= Math.max(this.ordinal, other.ordinal) }
                .toSet()
    }


}