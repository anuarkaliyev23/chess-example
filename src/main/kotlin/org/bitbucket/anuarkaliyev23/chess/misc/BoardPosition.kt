package org.bitbucket.anuarkaliyev23.chess.misc

import org.bitbucket.anuarkaliyev23.chess.NotationWritingUtils

data class BoardPosition(val vertical: Vertical, val horizontal: Horizontal) : Comparable<BoardPosition> {
    fun isBlackCell() = horizontal.numerical + vertical.numerical % 2 == 0

    override fun toString(): String {
        return "$vertical${horizontal.numerical}"
    }

    override operator fun compareTo(other: BoardPosition) : Int {
        return when {
            this.vertical != other.vertical -> vertical.compareTo(other.vertical)
            else -> when {
                this.horizontal != other.horizontal -> horizontal.compareTo(other.horizontal)
                else -> 0
            }
        }
    }

    companion object {
        fun of(notationWriting: String): BoardPosition = NotationWritingUtils.parse(notationWriting)
    }
}

