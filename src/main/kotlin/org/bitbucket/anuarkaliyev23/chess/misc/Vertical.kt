package org.bitbucket.anuarkaliyev23.chess.misc

enum class Vertical(val numerical: Int, val fullWidthChar: Char) {
    A(1, '\uff21'),
    B(2, '\uff22'),
    C(3, '\uff23'),
    D(4, '\uff24'),
    E(5, '\uff25'),
    F(6, '\uff26'),
    G(7, '\uff27'),
    H(8, '\uff28');

    operator fun minus(other: Vertical): Int {
        return this.numerical - other.numerical
    }

    operator fun plus(other: Vertical) : Int {
        return this.numerical + other.numerical
    }

    operator fun rangeTo(other: Vertical): Set<Vertical> {
        return Vertical
                .values()
                .filterIndexed {_, vertical ->  vertical.ordinal >= Math.min(this.ordinal, other.ordinal) && vertical.ordinal <= Math.max(this.ordinal, other.ordinal) }
                .toSet()
    }


}