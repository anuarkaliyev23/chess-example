package org.bitbucket.anuarkaliyev23.chess.game

import org.bitbucket.anuarkaliyev23.chess.exception.ChessException
import org.bitbucket.anuarkaliyev23.chess.misc.BoardPosition
import org.bitbucket.anuarkaliyev23.chess.misc.Horizontal
import org.bitbucket.anuarkaliyev23.chess.misc.Vertical
import org.bitbucket.anuarkaliyev23.chess.pieces.*
import java.lang.UnsupportedOperationException

data class ClassicGame(
        override val pieces: MutableSet<Piece>,
        override var isWhiteTurn: Boolean,
        override val moveHistory: MutableList<Move> = ArrayList()
) : Game {


    override fun handle(move: Move) {
        when(move) {
            is SimpleChessMove -> handleSimpleMove(move)
            is PawnTransformationMove -> handlePawnTransformation(move)
            is ShortCastlingMove -> handleShortCastling(move)
            is LongCastlingMove -> handleLongCastling(move)
            else -> throw UnsupportedOperationException()
        }
        moveHistory.add(move)
    }

    private fun handleSimpleMove(move: SimpleChessMove) {
        val piece = pieces.firstOrNull { move.position == it.position && move.pieceClass == it.javaClass  } ?: throw ChessException("No such piece")
        if (piece.isWhite != isWhiteTurn) throw ChessException("Not player's turn")

        if (piece.isPossibleMove(move.target)) {
            movePiece(piece, move.target)
            switchTurn()
        }
    }

    private fun handlePawnTransformation(move: PawnTransformationMove) {
        val piece = pieces.firstOrNull { move.position == it.position && move.pieceClass == it.javaClass  } ?: throw ChessException("No such piece")
        if (piece.isWhite != isWhiteTurn) throw ChessException("Not player's turn")

        val newPiece = Piece.instantiate(move.targetPieceClass, move.target, piece.hasMoved, piece.isWhite)
        pieces.remove(piece)
        pieces.add(newPiece)
        switchTurn()
    }

    private fun handleShortCastling(move: ShortCastlingMove) {
        val king = pieces.first { it.isWhite == isWhiteTurn && it is King }
        val rook = pieces.first { it.isWhite == isWhiteTurn && it is Rook && it.position.vertical == Vertical.H && !it.hasMoved }

        if (isWhiteTurn) {
            movePiece(king, BoardPosition.of("G1"))
            movePiece(rook, BoardPosition.of("F1"))

            switchTurn()
        } else {
            movePiece(king, BoardPosition.of("G8"))
            movePiece(rook, BoardPosition.of("F8"))
        }
    }

    private fun handleLongCastling(move: LongCastlingMove) {
        val king = pieces.first { it.isWhite == isWhiteTurn && it is King }
        val rook = pieces.first { it.isWhite == isWhiteTurn && it is Rook && it.position.vertical == Vertical.A && !it.hasMoved }

        if (isWhiteTurn) {
            movePiece(king, BoardPosition.of("C1"))
            movePiece(rook, BoardPosition.of("D1"))

            switchTurn()
        } else {
            movePiece(king, BoardPosition.of("C8"))
            movePiece(rook, BoardPosition.of("D8"))
        }
    }


    private fun switchTurn() {
        isWhiteTurn = !isWhiteTurn
    }

    private fun movePiece(piece: Piece, position: BoardPosition) {
        pieces.remove(piece)
        piece.position = position
        piece.hasMoved = true
        pieces.add(piece)
    }

    override operator fun get(position: BoardPosition) : Piece? {
        return pieces.firstOrNull { it.position == position }
    }

    override operator fun get(vertical: Vertical, horizontal: Horizontal) : Piece? {
        return get(BoardPosition(vertical, horizontal))
    }

    constructor() : this(generatePiecesLayout() as MutableSet<Piece>, true)

    companion object {
        fun generatePiecesLayout(isWhite: Boolean): MutableSet<Piece> {
            val pieces = HashSet<Piece>()
            val pawnHorizontal = if (isWhite) Horizontal._2 else Horizontal._7
            val mainPieceHorizontal = if (isWhite) Horizontal._1 else Horizontal._8

            Vertical.values().forEach { vertical ->
                val pawnPosition = BoardPosition(vertical, pawnHorizontal)
                pieces.add(Pawn(pawnPosition, false, isWhite))

                val position = BoardPosition(vertical, mainPieceHorizontal)
                when (vertical) {
                    Vertical.A, Vertical.H -> pieces.add(Rook(position, false, isWhite))
                    Vertical.B, Vertical.G -> pieces.add(Knight(position, false, isWhite))
                    Vertical.C, Vertical.F -> pieces.add(Bishop(position, false, isWhite))
                    Vertical.D -> pieces.add(Queen(position, false, isWhite))
                    Vertical.E -> pieces.add(King(position, false, isWhite))
                }
            }

            return pieces
        }

        fun generatePiecesLayout() : Set<Piece> {
            return generatePiecesLayout(true) + generatePiecesLayout(false)
        }
    }
}