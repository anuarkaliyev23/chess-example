package org.bitbucket.anuarkaliyev23.chess.game

import org.bitbucket.anuarkaliyev23.chess.misc.Horizontal
import org.bitbucket.anuarkaliyev23.chess.misc.Vertical

class GameConsoleGUI(
        val game: Game = ClassicGame()
) {

    fun show() {
        println(buildShow())
    }

    private fun buildShow(): String {
        val builder = StringBuilder()
        builder.append("\t")
        Vertical.values().forEach {
            builder.append("${it.fullWidthChar}\t")
        }

        builder.append("\n")
        Horizontal.values().forEach { horizontal ->
            builder.append("${horizontal.numerical}\t")
            Vertical.values().forEach { vertical ->
                val piece = game[vertical, horizontal]
                when (piece) {
                    null -> builder.append("\t")
                    else -> builder.append("${piece.unicodeSymbol()}\t")

                }
            }
            builder.append("\n")
        }

        return builder.toString()
    }
}