package org.bitbucket.anuarkaliyev23.chess.game

sealed class ChessGameStatus

data class Check(val toWhite: Boolean) : ChessGameStatus()
data class CheckMate(val toWhite: Boolean) : ChessGameStatus()
object Draw : ChessGameStatus()
object Common : ChessGameStatus()