package org.bitbucket.anuarkaliyev23.chess.game

import org.bitbucket.anuarkaliyev23.chess.NotationWritingUtils
import org.bitbucket.anuarkaliyev23.chess.exception.ChessException
import org.bitbucket.anuarkaliyev23.chess.exception.ImpossibleMoveException
import org.bitbucket.anuarkaliyev23.chess.exception.UnrecognizedPieceException
import org.bitbucket.anuarkaliyev23.chess.misc.BoardPosition
import org.bitbucket.anuarkaliyev23.chess.misc.Horizontal
import org.bitbucket.anuarkaliyev23.chess.pieces.King
import org.bitbucket.anuarkaliyev23.chess.pieces.Pawn
import org.bitbucket.anuarkaliyev23.chess.pieces.Piece
import org.bitbucket.anuarkaliyev23.chess.pieces.Rook
import java.util.*

sealed class Move {
    companion object {
        fun parse(notationWriting: String, game: Game): Move {
            if (notationWriting.trim() == NotationWritingUtils.LONG_CASLING_NOTATION) return LongCastlingMove(game)
            if (notationWriting.trim() == NotationWritingUtils.SHORT_CASTLING_NOTATION) return ShortCastlingMove(game)

            val tokenizer = StringTokenizer(notationWriting, " -")
            val firstPositionString = tokenizer.nextToken()
            val secondPositionString = tokenizer.nextToken()

            val transformationFigure = if (tokenizer.hasMoreTokens()) tokenizer.nextToken() else null

            val firstPosition = BoardPosition.of(firstPositionString)
            val targetPosition = BoardPosition.of(secondPositionString)

            val piece = game.pieces.firstOrNull { it.position == firstPosition } ?: throw UnrecognizedPieceException()

            if (!piece.isPossibleMove(targetPosition) || !possibleTrace(piece, targetPosition, game)) throw ImpossibleMoveException()
            if (piece is Pawn) { checkPawnEating(piece, targetPosition, game) }

            val tranformationFigureClass = if (transformationFigure == null) null else NotationWritingUtils.symbolToPieceClass(transformationFigure)

            return when (tranformationFigureClass) {
                null -> SimpleChessMove(
                        position = firstPosition,
                        target = targetPosition,
                        pieceClass = piece::class.java
                )
                else -> PawnTransformationMove(
                        position = firstPosition,
                        target = targetPosition,
                        pieceClass = piece::class.java,
                        targetPieceClass = tranformationFigureClass
                )
            }
        }

        private fun checkPawnEating(piece: Pawn, targetPosition: BoardPosition, game: Game) {
            if (targetPosition.vertical != piece.position.vertical ) {
                if (game.pieces.none { it.position == targetPosition && it.isWhite != piece.isWhite }) throw ImpossibleMoveException("You can move Pawn diagonally only if eating something")
            }
        }

        fun possibleTrace(piece: Piece, targetPosition: BoardPosition, game: Game): Boolean {
            val trace = piece.trace(targetPosition)
            if (trace.isEmpty()) return false
            if (trace.filter { it != piece.position && it != targetPosition }.any { position -> game.pieces.any {piece -> piece.position == position }}) {
                return false
            }

            val pieceOnTargetPosition = game.pieces.firstOrNull { it.position == targetPosition }
            if (pieceOnTargetPosition != null && pieceOnTargetPosition.isWhite == piece.isWhite) return false

            return true
        }
    }
}



data class SimpleChessMove (
        val position: BoardPosition,
        val target: BoardPosition,
        val pieceClass: Class<out Piece>
) : Move() {

    override fun toString() : String {
        return "${NotationWritingUtils.pieceClassToLetter(pieceClass)} $position - $target"
    }
}

data class PawnTransformationMove(
        val position: BoardPosition,
        val target: BoardPosition,
        val pieceClass: Class<out Piece> = Pawn::class.java,
        val targetPieceClass: Class<out Piece>
) : Move() {
    init {
        if (pieceClass != Pawn::class.java) throw ChessException("Only pawns can transform")
        if (target.horizontal != Horizontal._8 && target.horizontal != Horizontal._1) throw ImpossibleMoveException("Pawns can transform only on last horizontal")
    }

    override fun toString(): String {
        return "${NotationWritingUtils.pieceClassToLetter(pieceClass)} $position - $target ${NotationWritingUtils.pieceClassToLetter(targetPieceClass)}"
    }
}

data class ShortCastlingMove(val game: Game) : Move() {
    init {
        val king = game.pieces.first { it is King && it.isWhite == game.isWhiteTurn }
        val rooks = game.pieces.filter { it is Rook && it.isWhite == game.isWhiteTurn }

        val rook = (if (game.isWhiteTurn) {
            rooks.firstOrNull { it.position == BoardPosition.of("H1") }
        } else rooks.firstOrNull { it.position == BoardPosition.of("H8") }) ?: throw ImpossibleMoveException("No Rook at position")

        if (king.hasMoved) throw ImpossibleMoveException("King has moved!")
        if (rook.hasMoved) throw ImpossibleMoveException("Rook has moved!")

        val castlingFields = shortCastlingFields(game.isWhiteTurn)

        if (game.pieces.any{ piece -> castlingFields.contains(piece.position)} ) throw ImpossibleMoveException("There is pieces on path")

        val enemyPieces = game.pieces.filter { it.isWhite != game.isWhiteTurn }
        castlingFields.forEach {field ->
            for (piece in enemyPieces) {
                if (Move.possibleTrace(piece, field, game)) {
                    throw ImpossibleMoveException("Enemy $piece striking $field. Castling is impossible")
                }
            }
        }
    }

    override fun toString(): String {
        return NotationWritingUtils.SHORT_CASTLING_NOTATION
    }
}

private fun shortCastlingFields(isWhite: Boolean): Set<BoardPosition> {
    return when {
        isWhite -> setOf(BoardPosition.of("G1"), BoardPosition.of("F1"))
        else -> setOf(BoardPosition.of("G8"), BoardPosition.of("F8"))
    }
}

private fun longCastlingFields(isWhite: Boolean): Set<BoardPosition> {
    return when {
        isWhite -> setOf(BoardPosition.of("D1"), BoardPosition.of("C1"), BoardPosition.of("B1"))
        else -> setOf(BoardPosition.of("D8"), BoardPosition.of("C8"), BoardPosition.of("B8"))
    }
}


data class LongCastlingMove(val game: Game): Move() {
    init {
        val king = game.pieces.first { it is King && it.isWhite == game.isWhiteTurn }
        val rooks = game.pieces.filter { it is Rook && it.isWhite == game.isWhiteTurn }

        val rook = (if (game.isWhiteTurn) {
            rooks.firstOrNull { it.position == BoardPosition.of("H1") }
        } else rooks.firstOrNull { it.position == BoardPosition.of("H8") }) ?: throw ImpossibleMoveException("No Rook at position")

        if (king.hasMoved) throw ImpossibleMoveException("King has moved!")
        if (rook.hasMoved) throw ImpossibleMoveException("Rook has moved!")

        val castlingFields = longCastlingFields(game.isWhiteTurn)

        if (game.pieces.any{ piece -> castlingFields.contains(piece.position)} ) throw ImpossibleMoveException("There is pieces on path")

        val enemyPieces = game.pieces.filter { it.isWhite != game.isWhiteTurn }
        castlingFields.forEach {field ->
            for (piece in enemyPieces) {
                if (Move.possibleTrace(piece, field, game)) {
                    throw ImpossibleMoveException("Enemy $piece striking $field. Castling is impossible")
                }
            }
        }
    }

    override fun toString(): String {
        return NotationWritingUtils.LONG_CASLING_NOTATION
    }
}