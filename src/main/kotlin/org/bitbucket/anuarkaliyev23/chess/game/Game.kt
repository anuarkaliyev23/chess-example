package org.bitbucket.anuarkaliyev23.chess.game

import org.bitbucket.anuarkaliyev23.chess.misc.BoardPosition
import org.bitbucket.anuarkaliyev23.chess.misc.Horizontal
import org.bitbucket.anuarkaliyev23.chess.misc.Vertical
import org.bitbucket.anuarkaliyev23.chess.pieces.Piece

interface Game {
    val pieces: MutableSet<Piece>
    val isWhiteTurn: Boolean
    val moveHistory: MutableList<Move>

    fun handle(move: Move)

    operator fun get(position: BoardPosition) : Piece?
    operator fun get(vertical: Vertical, horizontal: Horizontal) : Piece?
}