package org.bitbucket.anuarkaliyev23.chess

import org.bitbucket.anuarkaliyev23.chess.exception.ChessException
import org.bitbucket.anuarkaliyev23.chess.misc.BoardPosition
import org.bitbucket.anuarkaliyev23.chess.misc.Horizontal
import org.bitbucket.anuarkaliyev23.chess.misc.Vertical
import org.bitbucket.anuarkaliyev23.chess.pieces.*

object NotationWritingUtils {
    fun parse(notationWriting: String): BoardPosition {
        val verticalChar = notationWriting[0]
        val horizontalChar = notationWriting[1].toString().toInt()

        val vertical = Vertical.valueOf(verticalChar.toString().toUpperCase())
        val horizontal = Horizontal.values().first{ it.numerical == horizontalChar }

        return BoardPosition(vertical, horizontal)
    }

    fun isCastling(notationWriting: String) : Boolean = notationWriting == SHORT_CASTLING_NOTATION || notationWriting == LONG_CASLING_NOTATION

    const val KING_SYMBOL = "K"
    const val QUEEN_SYMBOL = "Q"
    const val ROOK_SYMBOL = "R"
    const val BISHOP_SYMBOL = "B"
    const val KNIGHT_SYMBOL = "N"
    const val PAWN_SYMBOL = "P"

    const val SHORT_CASTLING_NOTATION = "0-0"
    const val LONG_CASLING_NOTATION = "0-0-0"

    fun symbolToPieceClass(letter: String) : Class<out Piece> {
        return when (letter) {
            KING_SYMBOL -> King::class.java
            QUEEN_SYMBOL -> Queen::class.java
            ROOK_SYMBOL -> Rook::class.java
            BISHOP_SYMBOL -> Bishop::class.java
            KNIGHT_SYMBOL -> Knight::class.java
            PAWN_SYMBOL -> Pawn::class.java
            else -> throw ChessException("Unrecognized Piece")
        }
    }

    fun pieceClassToLetter(piece: Class<out Piece>): String {
        return when (piece) {
            Pawn::class.java -> PAWN_SYMBOL
            Knight::class.java -> KNIGHT_SYMBOL
            Bishop::class.java -> BISHOP_SYMBOL
            Rook::class.java -> ROOK_SYMBOL
            Queen::class.java -> QUEEN_SYMBOL
            King::class.java -> KING_SYMBOL
            else -> throw ChessException("Unrecognized piece")
        }
    }
}