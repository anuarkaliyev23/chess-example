package org.bitbucket.anuarkaliyev23.chess.pieces

import org.bitbucket.anuarkaliyev23.chess.misc.BoardPosition
import java.util.*

data class Rook(override var position: BoardPosition,
                override var hasMoved: Boolean,
                override val isWhite: Boolean) : Piece {
    override val notationSymbol: String
        get() = "R"

    override fun unicodeSymbol(): Char {
        return when {
            isWhite -> '\u2656'
            else -> '\u265C'
        }
    }

    override fun trace(newPosition: BoardPosition): Set<BoardPosition> {
        val trace = LinkedHashSet<BoardPosition>()
        if (isPossibleMove(newPosition)) {
            if (newPosition.vertical != position.vertical) {
                for (vertical in newPosition.vertical..position.vertical) {
                    trace.add(BoardPosition(vertical, position.horizontal))
                }
            } else {
                for (horizontal in newPosition.horizontal..position.horizontal) {
                    trace.add(BoardPosition(position.vertical, horizontal))
                }
            }
        }
        return trace
    }

    override fun isPossibleMove(newPosition: BoardPosition): Boolean {
        if (Piece.samePosition(position, newPosition)) return false

        return (newPosition.vertical != position.vertical && newPosition.horizontal == position.horizontal)
                || (newPosition.vertical == position.vertical && newPosition.horizontal != position.horizontal)
    }

}