package org.bitbucket.anuarkaliyev23.chess.pieces

import jdk.nashorn.api.tree.Tree
import org.bitbucket.anuarkaliyev23.chess.misc.BoardPosition
import java.util.*

data class Knight(override var position: BoardPosition,
                  override var hasMoved: Boolean,
                  override val isWhite: Boolean) : Piece {
    override val notationSymbol: String
        get() = "N"

    override fun unicodeSymbol(): Char {
        return when {
            isWhite -> '\u2658'
            else -> '\u265E'
        }
    }


    override fun trace(newPosition: BoardPosition): Set<BoardPosition> {
        val trace = LinkedHashSet<BoardPosition>()
        if (isPossibleMove(newPosition)) {
            trace.add(newPosition)
        }
        return trace
    }


    override fun isPossibleMove(newPosition: BoardPosition): Boolean {
        if (Piece.samePosition(position, newPosition)) return false


        val vDiff = Math.abs(newPosition.vertical.numerical - position.vertical.numerical)
        val hDiff = Math.abs(newPosition.horizontal.numerical - position.horizontal.numerical)

        return (vDiff + hDiff) == 3
    }

}