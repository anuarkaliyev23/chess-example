package org.bitbucket.anuarkaliyev23.chess.pieces

import org.bitbucket.anuarkaliyev23.chess.misc.BoardPosition
import java.util.*

data class Pawn(override var position: BoardPosition,
                override var hasMoved: Boolean,
                override val isWhite: Boolean) : Piece {
    override val notationSymbol: String
        get() = "P"

    override fun unicodeSymbol(): Char {
        return when {
            isWhite -> '\u2659'
            else -> '\u265F'
        }
    }

    override fun trace(newPosition: BoardPosition): Set<BoardPosition> {
        val trace = LinkedHashSet<BoardPosition>()

        if (isPossibleMove(newPosition)) {
            trace.add(newPosition)
        }

        return trace
    }


    override fun isPossibleMove(newPosition: BoardPosition): Boolean {
        if (Piece.samePosition(position, newPosition)) return false

        if (newPosition.vertical == position.vertical) {
            if (Math.abs(newPosition.horizontal - position.horizontal) == 1) return true
            else if (!hasMoved && (Math.abs(newPosition.horizontal - position.horizontal)) == 2) return true
        } else {
            if (Math.abs(newPosition.vertical - position.vertical) == 1 && (Math.abs(newPosition.horizontal - position.horizontal) == 1)) return true
        }
        return false
    }
}