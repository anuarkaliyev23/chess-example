package org.bitbucket.anuarkaliyev23.chess.pieces

import jdk.nashorn.api.tree.Tree
import org.bitbucket.anuarkaliyev23.chess.misc.BoardPosition
import java.util.*

data class King(override var position: BoardPosition,
                override var hasMoved: Boolean,
                override val isWhite: Boolean) : Piece {
    override val notationSymbol: String
        get() = "K"

    override fun unicodeSymbol(): Char {
        return when {
            isWhite -> '\u2654'
            else -> '\u265A'
        }
    }

    override fun trace(newPosition: BoardPosition): Set<BoardPosition> {
        val trace = LinkedHashSet<BoardPosition>()
        if (isPossibleMove(newPosition)) {
            trace.add(newPosition)
        }
        return trace
    }

    override fun isPossibleMove(newPosition: BoardPosition): Boolean {
        if (Piece.samePosition(position, newPosition)) return false

        return (Math.abs(newPosition.vertical - position.vertical )<= 1 && Math.abs(newPosition.horizontal - position.horizontal) <= 1)
    }
}