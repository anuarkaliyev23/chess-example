package org.bitbucket.anuarkaliyev23.chess.pieces

import org.bitbucket.anuarkaliyev23.chess.misc.BoardPosition
import java.util.*
import kotlin.collections.LinkedHashSet

data class Bishop(override var position: BoardPosition,
                  override var hasMoved: Boolean,
                  override val isWhite: Boolean) : Piece {

    override val notationSymbol: String
        get() = "B"

    override fun unicodeSymbol(): Char {
        return when {
            isWhite -> '\u2657'
            else -> '\u265D'
        }
    }

    override fun trace(newPosition: BoardPosition): Set<BoardPosition> {
        val trace = LinkedHashSet<BoardPosition>()

        if (isPossibleMove(newPosition)) {
            var horizontals = position.horizontal..newPosition.horizontal
            var verticals = position.vertical..newPosition.vertical


            if (position.vertical > newPosition.vertical) {
                if (position.horizontal > newPosition.horizontal) {
                    horizontals = horizontals.sorted().toSet()
                    verticals = verticals.sorted().toSet()
                } else {
                    horizontals = horizontals.sorted().toSet()
                    verticals = verticals.sortedDescending().toSet()
                }
            } else {
                if (position.horizontal > newPosition.horizontal) {
                    horizontals = horizontals.sorted().toSet()
                    verticals = verticals.sortedDescending().toSet()
                } else {
                    horizontals = horizontals.sortedDescending().toSet()
                    verticals = verticals.sortedDescending().toSet()
                }
            }

            for (i in 0 until horizontals.size) {
                trace.add(BoardPosition(vertical = verticals.elementAt(i), horizontal = horizontals.elementAt(i)))
            }
        }
        return trace
    }

    override fun isPossibleMove(newPosition: BoardPosition): Boolean {
        if (Piece.samePosition(position, newPosition)) return false

        return (Math.abs(newPosition.vertical - position.vertical) == Math.abs(newPosition.horizontal - position.horizontal))
    }

}