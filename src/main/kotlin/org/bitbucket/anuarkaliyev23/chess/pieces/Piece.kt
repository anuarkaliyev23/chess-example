package org.bitbucket.anuarkaliyev23.chess.pieces

import org.bitbucket.anuarkaliyev23.chess.NotationWritingUtils
import org.bitbucket.anuarkaliyev23.chess.exception.ChessException
import org.bitbucket.anuarkaliyev23.chess.misc.BoardPosition
import java.util.*

interface Piece {
    var position: BoardPosition
    var hasMoved: Boolean
    val isWhite: Boolean
    val notationSymbol: String

    fun isPossibleMove(newPosition: BoardPosition) : Boolean
    fun trace(newPosition: BoardPosition) : Set<BoardPosition>
    fun unicodeSymbol() : Char

    companion object {
        fun samePosition(oldPosition: BoardPosition, newPosition: BoardPosition) = oldPosition == newPosition

        fun instantiate(pieceClass: Class<out Piece>, position: BoardPosition, hasMoved: Boolean, isWhite: Boolean) : Piece {
            return when (pieceClass) {
                King::class.java    -> King(position, hasMoved, isWhite)
                Queen::class.java   -> Queen(position, hasMoved, isWhite)
                Bishop::class.java  -> Bishop(position, hasMoved, isWhite)
                Knight::class.java  -> Knight(position, hasMoved, isWhite)
                Rook::class.java    -> Rook(position, hasMoved, isWhite)
                Pawn::class.java    -> Pawn(position, hasMoved, isWhite)

                else -> throw ChessException("Unrecognized piece")
            }
        }
    }
}