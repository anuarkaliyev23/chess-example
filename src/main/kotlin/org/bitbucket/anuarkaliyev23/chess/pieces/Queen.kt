package org.bitbucket.anuarkaliyev23.chess.pieces

import org.bitbucket.anuarkaliyev23.chess.misc.BoardPosition
import java.util.*

data class Queen(override var position: BoardPosition,
                 override var hasMoved: Boolean,
                 override val isWhite: Boolean) : Piece {
    override val notationSymbol: String
        get() = "Q"

    override fun unicodeSymbol(): Char {
        return when {
            isWhite -> '\u2655'
            else -> '\u265B'
        }
    }

    override fun trace(newPosition: BoardPosition): Set<BoardPosition> {
        val trace = LinkedHashSet<BoardPosition>()

        if (isPossibleMove(newPosition)) {
            val rook = Rook(position, hasMoved, isWhite)
            val bishop = Bishop(position, hasMoved, isWhite)

            return when {
                rook.isPossibleMove(newPosition) -> rook.trace(newPosition)
                else -> bishop.trace(newPosition)
            }
        }

        return trace
    }

    override fun isPossibleMove(newPosition: BoardPosition): Boolean {
        val rook = Rook(position, hasMoved, isWhite)
        val bishop = Bishop(position, hasMoved, isWhite)

        return rook.isPossibleMove(newPosition) || bishop.isPossibleMove(newPosition)
    }

}