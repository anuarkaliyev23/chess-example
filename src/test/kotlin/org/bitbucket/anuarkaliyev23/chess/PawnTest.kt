package org.bitbucket.anuarkaliyev23.chess

import org.bitbucket.anuarkaliyev23.chess.misc.BoardPosition
import org.bitbucket.anuarkaliyev23.chess.pieces.Pawn
import org.junit.jupiter.api.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class PawnTest {

    @Test
    fun isPossibleMove() {
        val pawn = Pawn(position = BoardPosition.of("e2"), hasMoved = false, isWhite = true)

        assertFalse { pawn.isPossibleMove(BoardPosition.of("e2")) }
        assertTrue { pawn.isPossibleMove(BoardPosition.of("e3"))}
        assertTrue { pawn.isPossibleMove(BoardPosition.of("e4"))}
        assertTrue { pawn.isPossibleMove(BoardPosition.of("d3"))}
        assertTrue { pawn.isPossibleMove(BoardPosition.of("f3"))}

    }
}