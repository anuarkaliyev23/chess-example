package org.bitbucket.anuarkaliyev23.chess

import org.bitbucket.anuarkaliyev23.chess.misc.BoardPosition
import org.bitbucket.anuarkaliyev23.chess.pieces.Bishop
import org.junit.jupiter.api.Test
import kotlin.test.assertTrue

class BishopTest {

    @Test
    fun isPossibleMove() {
        val bishop = Bishop(position = BoardPosition.of("d5"), hasMoved = false, isWhite = true)

        assertTrue { bishop.isPossibleMove(BoardPosition.of("e4")) }
        assertTrue { bishop.isPossibleMove(BoardPosition.of("f3")) }
        assertTrue { bishop.isPossibleMove(BoardPosition.of("g2")) }
        assertTrue { bishop.isPossibleMove(BoardPosition.of("h1")) }


        assertTrue { bishop.isPossibleMove(BoardPosition.of("e6")) }
        assertTrue { bishop.isPossibleMove(BoardPosition.of("f7")) }
        assertTrue { bishop.isPossibleMove(BoardPosition.of("g8")) }

        assertTrue { bishop.isPossibleMove(BoardPosition.of("c6")) }
        assertTrue { bishop.isPossibleMove(BoardPosition.of("b7")) }
        assertTrue { bishop.isPossibleMove(BoardPosition.of("a8")) }

        assertTrue { bishop.isPossibleMove(BoardPosition.of("c4")) }
        assertTrue { bishop.isPossibleMove(BoardPosition.of("b3")) }
        assertTrue { bishop.isPossibleMove(BoardPosition.of("a2")) }
    }

    @Test
    fun trace() {
        val bishop = Bishop(position = BoardPosition.of("d5"), hasMoved = false, isWhite = false)

        println(bishop.trace(BoardPosition.of("h1")))
        println(bishop.trace(BoardPosition.of("a8")))
    }
}