package org.bitbucket.anuarkaliyev23.chess

import org.bitbucket.anuarkaliyev23.chess.game.ClassicGame
import org.bitbucket.anuarkaliyev23.chess.game.GameConsoleGUI
import org.bitbucket.anuarkaliyev23.chess.game.SimpleChessMove
import org.bitbucket.anuarkaliyev23.chess.misc.BoardPosition
import org.bitbucket.anuarkaliyev23.chess.pieces.Pawn
import org.junit.jupiter.api.Test

class ClassicGameTest {

    @Test
    fun handle() {
        val game = ClassicGame()
        val gui = GameConsoleGUI(game)

        gui.show()
        val move = SimpleChessMove(
                position = BoardPosition.of("e2"),
                target = BoardPosition.of("e4"),
                pieceClass = Pawn::class.java
        )
        game.handle(move)

        gui.show()
    }
}