package org.bitbucket.anuarkaliyev23.chess

import org.bitbucket.anuarkaliyev23.chess.game.ClassicGame
import org.bitbucket.anuarkaliyev23.chess.game.GameConsoleGUI
import org.bitbucket.anuarkaliyev23.chess.game.Move
import org.bitbucket.anuarkaliyev23.chess.misc.BoardPosition
import org.bitbucket.anuarkaliyev23.chess.pieces.Pawn
import org.bitbucket.anuarkaliyev23.chess.pieces.Piece
import org.junit.jupiter.api.Test

class ChessMoveTest {

    @Test
    fun parse() {
        val notation = "e7 - e8 N"
        val pieces = HashSet<Piece>()
        pieces.add(Pawn(position = BoardPosition.of("e7"), hasMoved = true, isWhite = true))
        val game = ClassicGame(pieces = pieces, isWhiteTurn = true)
        val gui = GameConsoleGUI(game)

        gui.show()
        game.handle(Move.parse(notation, game))

        gui.show()
    }
}