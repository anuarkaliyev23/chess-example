package org.bitbucket.anuarkaliyev23.chess

import org.bitbucket.anuarkaliyev23.chess.misc.BoardPosition
import org.bitbucket.anuarkaliyev23.chess.pieces.Knight
import org.junit.jupiter.api.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class KnightTest {

    @Test
    fun isPossibleMove() {
        val knight = Knight(position = BoardPosition.of("b1"), hasMoved = false, isWhite = true)

        assertTrue { knight.isPossibleMove(BoardPosition.of("c3")) }
        assertTrue { knight.isPossibleMove(BoardPosition.of("a3")) }
        assertTrue { knight.isPossibleMove(BoardPosition.of("d2")) }
        assertFalse { knight.isPossibleMove(BoardPosition.of("b1")) }
    }
}
