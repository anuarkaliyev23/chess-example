package org.bitbucket.anuarkaliyev23.chess

import org.bitbucket.anuarkaliyev23.chess.misc.BoardPosition
import org.bitbucket.anuarkaliyev23.chess.misc.Horizontal
import org.bitbucket.anuarkaliyev23.chess.misc.Vertical
import org.bitbucket.anuarkaliyev23.chess.pieces.King
import org.junit.jupiter.api.Test
import kotlin.test.assertTrue

class KingTest {

    @Test
    fun isPossibleMove() {
        val king = King(position = BoardPosition.of("f5"), hasMoved = true, isWhite = true)

        val verticals = Vertical.values().filter { Math.abs(it - king.position.vertical) == 1 }
        val horizontals = Horizontal.values().filter { Math.abs(it - king.position.horizontal) == 1 }

        verticals.forEach {vertical ->
            horizontals.forEach { horizontals ->
                assertTrue { king.isPossibleMove(BoardPosition(vertical, horizontals)) }
            }
        }
    }
}