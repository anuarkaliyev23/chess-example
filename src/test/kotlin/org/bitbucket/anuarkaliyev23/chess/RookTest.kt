package org.bitbucket.anuarkaliyev23.chess

import org.bitbucket.anuarkaliyev23.chess.misc.BoardPosition
import org.bitbucket.anuarkaliyev23.chess.misc.Horizontal
import org.bitbucket.anuarkaliyev23.chess.misc.Vertical
import org.bitbucket.anuarkaliyev23.chess.pieces.Rook
import org.junit.jupiter.api.Test
import kotlin.test.assertTrue

class RookTest {

    @Test
    fun isPossibleMove() {
        val rook = Rook(position = BoardPosition.of("d5"), hasMoved = true, isWhite = true)

        Vertical.values().filter { it != Vertical.D }.forEach { vertical ->
            assertTrue { rook.isPossibleMove(BoardPosition(vertical = vertical, horizontal = rook.position.horizontal)) }
        }

        Horizontal.values().filter { it != Horizontal._5 }.forEach { horizontal ->
            assertTrue { rook.isPossibleMove(BoardPosition(vertical = rook.position.vertical, horizontal = horizontal)) }
        }
    }


    @Test
    fun trace() {
        val rook = Rook(position = BoardPosition.of("H8"), hasMoved = true, isWhite = true)

        println(rook.trace(BoardPosition.of("G1")))
    }
}